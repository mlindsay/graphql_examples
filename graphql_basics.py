# Simple example that demonstrates a possible way of doing the CURL example here;
# https://docs.gitlab.com/ee/api/graphql/getting_started.html#command-line
# It requires a valid Personal Access Token set as an env var called 'TOKEN'

import json
import os
import requests

# Need an environment variable set to a valid token
try:
    graphql_token = os.environ['TOKEN']
except:
    print("You should have a valid TOKEN environment variable")

url = 'https://gitlab.com/api/graphql' # Replace gitlab.com with your instance name

# Setting up our headers
headers = {
    "Authorization": "bearer " + graphql_token,
    "Content-Type": "application/json"
}

# Setting up our query
query = """
{
    currentUser {
        name
    }
}
"""

# Performing the request
def run_query(query):
    '''
    Simple function to post to url with the query string. If we get a
    status_code 200 return a json body.  Important to note, this will
    return 200 for an authentication failure.
    '''
    r = requests.post(url, json={'query': query}, headers=headers)
    if r.status_code == 200:
        return r.json()
    else:
        raise Exception("Something went wrong\nstatus_code: {}\nquery:\n{}".format(r.status_code, query))

print(run_query(query))
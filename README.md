# graphql usage and benefits

Simple project to highlight some differences between graphql and rest.

# Example runtimes

This was done using graphql, pulling back iid, title, and state against the group gitlab-com repo.

![alt text](img/graphql_example.png) "Real world example")

#### TODO
- [ ] Count issues for a given group and its sub-groups using graphql.
- [ ] Count issues for a given group and its sub-groups using rest.
- [ ] Measure time required for graphql.
- [ ] Measure time required for rest.
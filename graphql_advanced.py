# advanced examples that demonstrates using queries from the graphiql interface here;
# https://gitlab.com/-/graphql-explorer
# These examples generally would not be appropriate for CLI, due to quoting
# issues with the JSON.

import json
import os
import re
import requests
from time import perf_counter # basic stop watch.
from sys import getsizeof # see how big our actual variable is

# start the stopwatch
start_time = perf_counter()

# Need an environment variable set to a valid token
try:
    graphql_token = os.environ['TOKEN']
except:
    print("You should have a valid TOKEN environment variable")

url = 'https://gitlab.com/api/graphql' # Replace gitlab.com with your instance name
group = 'your-group-name-here'

# Setting up our headers
headers = {
    "Authorization": "bearer " + graphql_token,
    "Content-Type": "application/json"
}

# More complex example that would be hard to do using curl without a lot of 
# quoting and escaping shenanigans. Because I'm using a python f-string, I have
# to double up on the curly braces, but this does allow for variable
# substitution in the string literal.
query = f"""
{{
  group(fullPath: "{group}") {{
    id
    name
    issues(first: 500) {{
      nodes {{
        iid
        title
        state
      }}
      pageInfo {{
          hasNextPage
          endCursor
        }}
    }}
  }}
}}
"""

# Performing the request
def run_query(query):
    '''
    Simple function to post to url with the query string. If we get a
    status_code 200 return a json body.  Important to note, this will
    return 200 for an authentication failure.
    '''
    issues = {'no_calls': 0, 'issues': []}

    while True:
      r = requests.post(url, json={'query': query}, headers=headers)
      issues['no_calls'] += 1
      if r.status_code == 200 and 'data' in r.json(): # data means we got a valid response from the server
        for issue in r.json()['data']['group']['issues']['nodes']:
          issues['issues'].append(issue)
          if r.json()['data']['group']['issues']['pageInfo']['hasNextPage'] != False:
            next_cursor = r.json()['data']['group']['issues']['pageInfo']['endCursor']
            query = re.sub('\(first: 500.*\)', '(first: 500 after: "{}")'.format(next_cursor), query)
          else:
            return issues
            break
      else:
          print(f"LAST JSON:\n{ r.json() }")
          print(f"QUERY:\n{ query }")
          break

# find all the issues in a group with sub-groups
records = run_query(query)
# no_records = records['data']['group']['issues']['nodes']

stop_time = perf_counter()
actual_time = stop_time - start_time

# print()
# for issue in records['issues']:
#   print(issue)

message = f"""
It took { round(actual_time, 4) } seconds for the graphql to run
We returned { len(records['issues']) } records.
We made { records['no_calls'] } graphql API calls.
Our returned records occupied {getsizeof(records['issues'])} bytes of memory.
"""

print(message)